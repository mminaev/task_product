package main

import (
	"flag"
	"encoding/csv"
	"os"
	"fmt"
	"strconv"
	"strings"
	"log"
	"sort"
	"io"
)

type product struct {
	name string
	price int64
}

type products = []product

func readProducts(path string) (products, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	reader := csv.NewReader(file)

	var out products
	i := 0
	for ;;i++ {
		record, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				return out, nil
			}
			return nil, err
		}
		if len(record) != 2 {
			return nil, fmt.Errorf("unexpected len of csv record: expect 2 have: %d on line: %d", len(record), i + 1)
		}

		price, err := strconv.ParseInt(record[1], 10, 64)
		if err != nil {
			return nil, fmt.Errorf("can't parse price: %v on line: %d", err, i + 1)
		}
		if price <= 0 {
			return nil, fmt.Errorf("bad price, price must be > 0 on line: %d", i + 1)
		}
		out = append(out, product{name: strings.TrimSpace(record[0]), price: price})
	}
}

func main()  {
	sum := flag.Int64("sum", 200, "total money")
	productsFile := flag.String("products", "./products.csv", "path to file with data in csv format")

	flag.Parse()

	products, err := readProducts(*productsFile)
	if err != nil {
		log.Fatal(err)
	}

	sort.Slice(products, func(i, j int) bool {
		return products[i].price > products[j].price
	})

	totalMoney := int64(0)
	for _, product := range products {
		if product.price <= *sum {
			*sum -= product.price
			fmt.Printf("%s - %d\n", product.name, product.price)
			totalMoney += product.price
		}
	}
	fmt.Printf("Total: %d\n", totalMoney)
}